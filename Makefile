#!/usr/bin/make -f
.PHONY: all
all: ctn_algaas_paper

ctn_algaas_paper: %: %.tex
	latexmk -f -pdf $@

.PHONY: clean
clean:
	latexmk -c

.PHONY: cleanall
cleanall:
	latexmk -C
