# CTN Lab: AlGaAs/GaAs Coatings Brownian Noise Measurement Paper
[![pipeline status](https://git.ligo.org/cit-ctnlab/ctn_paper/badges/master/pipeline.svg)](https://git.ligo.org/cit-ctnlab/ctn_paper/commits/master)

This repo will be used to write journal paper for the results of direct
measurement of coatings' brownian noise for AlGaAs/GaAs coated mirrors done
in CTN lab at Caltech.

## Compiled Paper:
[ctn_algaas_paper.pdf](https://git.ligo.org/cit-ctnlab/ctn_paper/-/jobs/artifacts/master/file/ctn_algaas_paper.pdf?job=make)

## Contribution guidelines
Please see [CONTRIBUTING](./CONTRIBUTING.md)

## Running code for figure plotting

* Run following to create cit_ctn conda environment:
```
cd code
conda env create --file cit_ctn_algaas_conda_end.yml
```
* Activate cit_ctn environment and create ipython kernel with it:
```
conda activate cit_ctn
python -m ipykernel install --user --name=cit_ctn
```
* While opening jupyter notebooks, change kernel to cit_ctn by dropdown menu:
Kernel>Change Kernel>cit_ctn (if not already selected)
