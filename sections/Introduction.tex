\section{Introduction}
\label{sec:introduction}
The Advanced Laser Interferometer Gravitational-wave Observatory (AdvLIGO) has
been detecting gravitational-wave signals originating from merging binary black
holes and binary neutron stars ~\cite{Abbott2019}. The astounding
$10^{-19}\text{m}/\sqrt{\mathrm{Hz}}$ displacement sensitivity is achieved with
4-km arm-length dual-recycled Michelson interferometers with Fabry-Perot
cavities ~\cite{Abbott2016}. To keep high circulating power and low loss in the
arms, the test mass mirrors are coated with alternate quarter wavelength layers
of high and low refractive indices, resulting in a high reflectivity surface.
Further, optimization\cite{Kimble2008, Gorodetsky2008} on individual layer
thicknesses is done to cancel thermo-optic noise.

Presently these mirrors are made of alternate layers of amorphous silica and
titania-doped tantala~\cite{Harry2006}. While this reduced the noise floor in
AdvLIGO substantially, the 10-500 Hz region of the design sensitivity in
AdvLIGO is limited by the coatings Brownian noise~\cite{Martynov2016}. Coatings
Brownian noise is the result of fluctuations in the material by virtue of it
being at a finite temperature. Various methods are being pursued to reduce this
noise~\cite{Steinlechner2018}. These include new coating
structures~\cite{Yam2015, Steinlechner2015, Steinlechner2016, Pan2014} and
novel materials~\cite{Steinlechner2018aSi, Pan2018, Cumming2015, Cole2013} for
the coating. The fluctuation-dissipation theorem relates the energy dissipation
in a mechanical system with its equilibrium noise
fluctuations~\cite{Callen1952}. The disordered structure of amorphous materials
leads to mechanical loss peaks at low temperatures~\cite{Pohl2002}. Hence, it
is suggested that crystalline materials might be able to provide lower Brownian
noise solutions.

Cole et al.~\cite{Cole2013} fabricated a coating with crystalline \AlGaAs~and
GaAs, and used substrate-transfer with optical bonding to apply the coating to
a fused silica substrate. Preliminary results suggest that this material choice
can improve Brownian noise performance. Chalermsongsak et al.~\cite{Tara2016}
optimized the coating structure made with these materials to achieve coherent
cancellation of thermo-optic noise and demonstrated it as well. We used the
same experimental setup with improvisations for better sensitivity to measure
the coatings Brownian noise for these thermo-optic noise optimized
(TO-optimized) coatings.

Penn et al.~\cite{Penn2019} performed indirect noise measurement for this
coating structure by doing mechanical ring-down measurements of silica discs,
with and without these coatings applied. This provides an estimate of the loss
angles of the coating. Our experiment complements this work by directly
measuring the phase noise of reflected light due to these coatings and using
the theoretical model of Hong et al.~\cite{Hong2013} to infer the same loss
angles. This helps us not only in generating more information about these
particular crystalline coatings but also understanding the nature of actual
phase noise reflected by optically bonded coatings with small beam areas and
verifying the theoretical understanding we have so far.

In Section~\ref{sec:experiment}, we describe our experimental setup in detail
focusing on the design of test cavities and control loops. Then in
Section~\ref{sec:noise_budget} we describe each noise contribution, its origin
, and calculation in detail. One of the aims of this paper is to create a
reference for all these calculations for noise budgeting in other similar
experiments. Then in Section~\ref{sec:results} we report our measured noise
spectrum and the inferred loss angle. Finally, in Section~\ref{sec:discussion},
we discuss the projected noise for AdvLIGO with our coatings, compare our
results with the indirect measurements, and discuss the possible reasons for
the excess noise measured by us.
