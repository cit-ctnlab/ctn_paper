\section{Noise budget for the experiment}
\label{sec:noise_budget}
In this section, we describe the various sources of noise we have taken into
account in inferring the coatings Brownian noise from our measurements.
Fig.~\ref{fig:noise_sources} illustrates most of the contributing noise sources
that are indistinguishable from the coatings Brownian noise. All the symbols
used in the equations in this section are explained in
Table.~\ref{tab:nb_symbols}. Noise power spectral densities (PSD) are denoted
as $S_\text{cause}$. When a superscript of `$x$' is present, it denotes cavity
length noise PSD. A conversion factor of $(c/(L\lambda))^2$ is applied to
convert cavity length variations to absolute laser frequency noise sampled by
our beatnote readout.

\input{sections/tableTex/nb_symbols}

\input{sections/figureTex/NoiseSources}

%______________________________________________________________________________
\subsection{Coatings Thermo-Optic Noise}
\label{subsec:coatTO}
Coatings thermo-optic noise is typically a dominant noise source in mirror
coatings. When a gaussian beam of a laser falls on a mirror surface, stress
inhomogeneity causes fluctuations in the temperature~\cite{Zener1938}. Due to
thermo-elasticity, the thickness of each layer fluctuates with the temperature,
proportional to their coefficient of thermoelasticity (CTE) $\alpha_j$ of the
$j^{th}$ layer. The same thermal fluctuations also induce changes in the
refractive indices of each layer that is proportional to their coefficients of
thermo-refractivity (CTR) $\beta_j$.

As shown in Evans et al., since these two pathways of noise are driven by the
same noise source, they are coherently added such that total thermo-optic noise
is given by \cite[Eq. 4.]{Evans2008}:
\begin{equation}
S_{coatTO}^x = \Gamma_{tc}
               S^{\Delta T} (\bar{\alpha_\text{c}}d
                             - \bar{\beta}\lambda
                             - \bar{\alpha_\text{s}}
                               d \frac{C_\text{c}}{C_\text{s}})
\label{eq:coatTO}
\end{equation}
where $\bar{\alpha_\text{c}}$ and $\bar{\beta}$ are effective compound CTE and
CTR for the coating respectively, $\bar{\alpha_\text{s}}$ is the effective CTE
for the substrate, and $\Gamma_{tc}$ is the thick coating correction factor
(see App.~\ref{app:coatTO}). $S^{\Delta T}$ is single-sided gaussian beam
profile weighted temperature fluctuation PSD given by (see
App.~\ref{app:tempPSD}):
\begin{subequations}
\begin{align}
S^{\Delta T} &= \frac{2^{3/2} k_\text{B} T^2}
                    {\pi\kappa_\text{s}w} M(f/f_\text{T})\\
   M(\Omega) &= Re\left[
                         \int\limits_0^\infty\!
                                \mathrm{d}u\,
                                \frac{u\ \mathrm{e}^{-u^2/2}}
                                     {\left(u^2-\mathrm{i}\Omega\right)^{1/2}}
                  \right]
\end{align}
\label{eq:tempPSD}
\end{subequations}

The negative sign in Eq.~\ref{eq:coatTO} means an optimization can be carried
out to make these two coupling mechanisms cancel each other. The mirrors we are
testing have a coating structure that is optimized to minimize this noise. This
was experimentally demonstrated in previously published work\cite{Tara2016}.

%______________________________________________________________________________
\subsection{Coatings Brownian Noise}
\label{subsec:coatBr}

\input{sections/figureTex/qBk_qSk}

Once coatings thermo-optic noise is reduced, the predominant remaining noise
source in mid-range frequencies is coatings Brownian noise. Even with no
temperature fluctuations, there is microscopic motion happening in the material
driven by its finite temperature. For a solid material, one can think of this
as small vibrations of particles in the material due to phonon excitations that
happen as equilibrium sets with the coupling to the thermal bath of the
environment.

We followed the analysis presented in Hong et al.\cite{Hong2013} to estimate
the coatings Brownian noise. The finite room temperature induces stress noise
fields in bulk and shear modes of the material in the layers of the coatings. A
transfer function is calculated from such stress fields to surface heights and
layer thicknesses of the coating layers. These fluctuations are carried forward
to the phase noise of reflected light due to the compound reflection from all
the layers of the mirror.

Characterization of Brownian noise is done through fluctuation-dissipation
theorem~\cite{Callen1952}. This states that the more damped a system is, the
stronger an energy transfer channel it has with the environment and hence more
fluctuations are observed in the equilibrium position. So a measure of energy
dissipation in the mechanical structure of the material is a quantitative
characterization of how good that material is in terms of Brownian noise.

This measure comes by assuming an imaginary part in the Bulk and Shear modulus
of the material of layers called ``Bulk and Shear Loss Angles" in the
literature \cite[Eq. 1.]{Hong2013}.
\begin{equation}
\bar{K} = K(1 + i\Phi_B),\quad \bar{\mu} = \mu(1 + i\Phi_S)
\end{equation}
The single-sided power spectral density of the reflected light's phase noise is
directly proportional to these loss angles \cite[Eq. 94, 96]{Hong2013}:
\begin{subequations}
\begin{equation}
S^x_\text{coatBr} = \sum_j \left(q_j^B S_j^B + q_j^S S_j^S\right)
\end{equation}
\begin{equation}
S_j^X = \frac{4 k_B T \lambda \Phi_j^X (1 - \sigma_j - 2\sigma_j^2)}
             {3 \pi f n_j Y_j (1 - \sigma_j)^2 \mathcal{A}_{eff}}
             ,\quad\quad X = B,S
\end{equation}
\end{subequations}

Here, $q_j^B$ and $q_j^S$ (plotted in Fig.~\ref{fig:qBk_qSk}) are each layers'
contribution transfer functions to the phase noise of light reflected from Bulk
or Shear stress noise fields respectively (see App.~\ref{app:coatBr} for
details).

Note that Hong et al.\cite{Hong2013} also consider amplitude noise imparted on
light due to the Brownian noise which gets converted into phase noise due to
radiation pressure. We have ignored this effect as our mirrors are fixed having
effectively infinite mass.

%______________________________________________________________________________
\subsection{Substrate Brownian Noise}
\label{subsec:subBr}
Through the same mechanism as coatings Brownian noise, the substrate also
imparts phase noise on the light due to mechanical loss and finite temperature.
However, fused silica has a very low mechanical loss, and multiple layers in
the coating multiply the effect of coatings Brownian noise. Hence substrate
Brownian noise is not as dominant as compared to coatings Brownian noise. Out
noise model includes this source as:
\begin{equation}
S^x_\text{subBr} = \frac{2 k_\text{B} T}{\pi^{3/2} f}
                    \frac{1-\sigma_\text{s}^2}{w E_\text{s}}\Phi_\text{s}
\end{equation}

%______________________________________________________________________________
\subsection{Substrate Thermoelastic Noise}
\label{subsec:subTE}
Temperature fluctuations cause fluctuations in the thickness of the substrate
as well. Since most of the reflection happens at the coatings, the effect of
this noise is reduced. We followed the analytical expression by Somiya et
al.~\cite[Eq. 3, 8]{Somiya2010} to calculate this noise contribution:
\begin{subequations}
\begin{equation}
S^x_\text{subTE} = \frac{4 k_\text{B} T^2}{\pi^{1/2}}
                   \frac{\alpha_\text{s}^2 (1+\sigma_\text{s})^2 w}
                        {\kappa_\text{s}}
                   J(f/f_\text{T})
\end{equation}
\begin{equation}
\begin{aligned}
J(\Omega) = &Re \left[
                  \frac{\mathrm{e}^{i\Omega/2} (1 - i\Omega)}
                       {\Omega^2}
                       \left(
                       \operatorname{Erf}\left[
                                           \frac{\sqrt{\Omega}(1+i)}{2}
                                         \right]
                        - 1
                       \right)
                \right] \\
            &+ \frac{1}{\Omega^2} - \sqrt{\frac{1}{\pi \Omega^3 }}
\end{aligned}
\end{equation}
\end{subequations}

\input{sections/figureTex/CTN_Noise_Budget}

%______________________________________________________________________________
\subsection{Technical Noise Sources}
%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Seismic Noise}
\label{subsubsec:seismic}
Low-frequency acceleration of the experimental apparatus couples into the
bending motion of the cavities as shown in Fig.~\ref{fig:noise_sources}. The
bending of the cavity leads to changes in the longitudinal length of the cavity
which directly affects the readout frequency from the cavity. This technical
noise coupling is minimized by placing the cavity supports at the Airy points,
and verifying the same with FEA simulations \cite{TaraThesis}. If we assume
mounting errors of $\pm 0.5$ mm and common-mode rejection due to mounting the
two cavities on a common platform, the coupling from acceleration into cavity
strain is estimated to be $6\times10^{-12}\,m^{-1}\,s^{2}$. The brown curve in
Fig.~\ref{fig:ctn_nb} shows the estimated coupled seismic noise from measured
seismic vertical acceleration in our laboratory.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Sensing Noise}
Finally, the digital phase-locked loop (DPLL) used to readout the time series
of beat note frequency injects its own frequency noise. Any frequency reading
device can only read frequency noise as well as its own reference oscillator.
With rubidium clock stabilization, the frequency noise of our DPLL was found to
be less than 1 mHz/$\sqrt{\text{Hz}}$ up to 4 kHz, which was measured by
generating a 27.344 MHz signal by Moku and fed to itself through a long cable
for frequency measurement. Along with this, the beatnote detector's dark and
shot noise also contributes to the sensing noise. The green curve in
Fig.~\ref{fig:ctn_nb} shows the contribution of this noise in the measurement.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Controls Shot Noise}
Shot noise in the resonant RF Photodiodes used in the PDH loop for FSS (see
Sec.~\ref{subsec:fss}) adds noise to the frequency of the laser. We ensure that
enough light is falling on these detectors to keep them shot noise limited
rather than dark noise limited. For total power $P_0$ incident on the cavity,
the PDH shot-noise is estimated by:
\begin{equation}
\begin{aligned}
S_{PDHshot} &= \left(\frac{f_p}{2 P_0 m}\right)^2
               \left(1 + \frac{f}{f_p} \right)^2 \\
            &\quad
               \left(
                  2h\nu P_0 \left[
                                J_0(\Gamma)^2 (1-\eta)
                                + 3 J_1(\Gamma)^2
                            \right]
               \right)
\end{aligned}
\end{equation}
Here, the first term in parenthesis is the PDH discriminant term with units of
Hz/W, where $m$ is the modulation index of sidebands used for PDH. The second
term in parenthesis accounts for cavity pole $f_p$ and the third term is simply
the shot noise for the total power falling on the detectors including carrier
and sidebands. Overall, this noise goes down as we increase the power of the
laser. The grey curve in Fig.~\ref{fig:ctn_nb} shows the contribution of this
noise in our experiment as negligible in all of the frequency ranges.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Laser Frequency Noise}
NPRO lasers are inherently low linewidth as they are stabilized with a
temperature-controlled crystal cavity in a monolithic non-planar geometry. The
free-running frequency noise ASD Nd:YAG NPRO is assumed to be 1
kHz/$\sqrt{\text{Hz}}$ at 10 Hz and assumed to fall as $1/f$ at higher
frequencies \cite{Willke2000}. We measured the frequency noise suppression of
our FSS (see Sec.~\ref{subsec:fss}) and applied that to get the estimated
residual frequency noise of the laser in the transmission from the cavities.
The magenta curve shows the suppressed laser frequency noise contribution in
the beat note frequency noise.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
\subsubsection{Laser Amplitude Noise}
\label{subsubsec:photothermal}
Some absorption of light always happens at the coatings. This means that the
coating gets heated due to the incident power on it. In the case of a cavity,
this is the circulating power inside the cavity. Therefore, any fluctuations in
the intensity of light drive fluctuations in the temperature of the coating.
Since the overall heat capacity of coatings is small, the delay in temperature
rise/fall due to intensity rise/fall is negligible and a near-instant response
is seen in the measurement frequency band. Since this is another source of
temperature fluctuations, the phase noise follows the same thermo-optic pathway
described in Sec.~\ref{subsec:coatTO} and can be written as:
\begin{equation}
S^x_\text{photoThermal} = |H(f)|^2 P^2_{abs} S_{RIN}
\end{equation}
where $H(f)$ is the complex photothermal transfer function (see
App.~\ref{app:photoThermal}) for a mirror, $P_{abs} = a_{coat}P_{circ}$ is
power absorbed by the mirror and $S_{RIN}$ is the PSD of relative intensity
noise (RIN) of the incident laser. The blue curve in Fig.~\ref{fig:ctn_nb}
shows the contribution of this noise source in the beat note frequency noise.
