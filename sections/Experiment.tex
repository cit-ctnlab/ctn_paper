\section{Description of experiment}
\label{sec:experiment}

Fig.~\ref{fig:complete_schematic} shows a complete schematic of the experiment,
showing beam paths and various control loops involved. In this section, we
describe our experimental setup for delineating parameters and establishing
reproducibility.

\input{sections/figureTex/ctn_schematic}

%______________________________________________________________________________
\subsection{Test cavities}
\label{subsec:test_cavities}

\input{sections/figureTex/cav_support}

The mirrors under test are used to make two identical cavities. The mirrors are
optically bonded to fused silica spacers. The properties of these cavities are
listed in Table.~\ref{tab:cavity_params}. The mirrors are made with 57 layers
composed of alternating GaAs and \AlGaAs. The layer thicknesses have been
optimized and experimentally verified to show the cancellation of thermo-optic
noise in the coatings~\cite{Tara2016}. See Fig.~\ref{fig:CoatStruct} for the
optical thickness profile of the coating layers. This coating has been applied
in an 8~mm diameter region at the center of the mirrors and an annulus of about
3~mm is left clean at the edges of the mirrors for proper optical contact.

\input{sections/figureTex/CoatingStructure}

\input{sections/tableTex/cavity_params}

The cavities are supported at their Airy points by four supports as shown in
Fig.~\ref{fig:supports}. Each support is cut from a cylindrical piece of
Polyether ether ketone (PEEK) to have as close to point contact as possible.
The Airy points were found to minimize the coupling of seismic noise into the
cavity longitudinal length changes\cite{TaraThesis}.

%______________________________________________________________________________
\subsection{Frequency Stabilization Servo}
\label{subsec:fss}

\input{sections/figureTex/FSS_OLTFs}

Fig.~\ref{fig:complete_schematic} shows some parts of the frequency
stabilization servo (FSS) used to lock the two NPRO lasers independently to the
two cavities. This is a critical element of the experiment as this control loop
ensures that our laser frequency tracks any minute changes in cavity length
without getting saturated by intrinsic noise up to roughly 200 kHz.

Sidebands of 36 MHz and 37 MHz are created via broadband New Focus 4004
Electro-Optics Modulators (EOM) with a modulation depth of approximately 0.3
radians in the North and South paths respectively. These EOMs were driven by
custom-built resonant EOM drivers, amplifying clean modulation signal taken
from Oven Controlled Crystal Oscillators (Custom parts from Wenzel Associates
Inc. with less than $165~\text{dBc}/\sqrt{\text{Hz}}$ phase noise at 1 kHz).

A conventional PDH setup is used to read the error signal by separating
reflection signals from cavities using Faraday isolators (FI in
fig.~\ref{fig:complete_schematic}). Custom-built resonant photodiodes with both
active and passive notches at 2-Omega frequencies are used to read the error
signals (here Omega refers to phase modulation frequency). It was found that
rejecting the 2-Omega frequency by a margin of about 46 dB with respect to the
resonant 1-Omega peak is essential to ensure the linearity of mixers and to
avoid slew rate saturation in the electronics downstream. This ensures the
smooth performance of FSS without saturation of any of the actuators.

The main servo for this control loop uses the LIGO $3^{\text{rd}}$ generation
Table Top Frequency Stabilization Servo~\cite{TTFSS}. There are two outputs
from this servo, one for NPRO laser PZT which carries the noise suppression
load up to about 10 kHz, and second, for an EOM placed upstream in the path for
fast actuation enabling unity gain frequencies up to 200 kHz and 300 kHz for
the North and South paths respectively. The RMS value of the PZT actuation
signal is also calculated in analog electronics and sent to an ADC. A script
samples the low-frequency drift of PZT actuation signal which is fed back to
actuate on the temperature of NPRO crystal for slow and coarse correction
fixing the DC offset of NPRO PZT. This maximizes the available dynamic range
for the actuator and hence the bandwidth. Fig.~\ref{fig:FSS_OLTFs} shows the
full open-loop transfer function for this control loop including all the
actuators resulting in suppression of frequency noise of free-running NPRO to
the order of $10^5$ in the frequency region of interest.

%______________________________________________________________________________
\subsection{Beatnote Detection and Readout}
\label{subsec:bndet}
The transmitted laser beams from the cavity carry displacement noise of the
mirror surface as frequency noise because of the strong PDH locking explained
above. The main purpose of having two equivalent paths is so that we can refer
them against each other to measure their differential cavity length noise
imprinted on their optical frequencies. The two beams interfere with each other
on a 50:50 beamsplitter and one of the outputs is read by a custom-built
resonant photodiode.

The resulting heterodyne beatnote is tracked by a digital phase-locked loop
(DPLL) with 10 kHz bandwidth. This DPLL is implemented on an FPGA inside Liquid
Instruments' Moku which in turn records the beatnote frequency at 15.625 kSa/s.
The digital implementation allowed for a larger dynamic range readout of the
frequency with high precision. The clock of Moku was referenced to a commercial
Rubidium atomic clock 10 MHz signal for reducing sensing frequency noise and
increasing stability over long durations.

To ensure the PLL tracked the heterodyne signal with good linearity, we made
sure that the beatnote frequency did not drift more than the bandwidth
capabilities during the measurement time, which was typically 960 s. For this,
the cavities were enclosed inside a vacuum can that was thermally insulated
with 2-inch CERTIFOAM 25. The temperature of the vacuum can is monitored with a
AD590 sensor through custom-built low noise temperature sensing circuits and
controlled with OMEGALUX silicone rubber heaters wrapped around the vacuum can.
This provides temperature stability of $\pm10~\text{mK}$ in the cavity
environment.

The cavities are wrapped by nichrome wires which are used to actuate length
changes to the spacer by heating. A beatnote frequency is captured before
cavities right after the laser heads and read by a Mini-Circuits UFC-6000
frequency counter. This pre-cavity beatnote frequency is used by a PID script
to control and limit the relative temperature of the cavities by controlling
the currents in the nichrome wires via a custom-built MOSFET current driver
circuit. All these efforts resulted in a stable beatnote frequency with a drift
of less than 1 kHz in more than 20 minutes.

%______________________________________________________________________________
\subsection{Other control loops}
\label{subsec:other_control_loops}
Apart from stabilizing the frequency of the laser at the lock point, we
controlled the relative intensity noise of the lasers as well (See
sec.~\ref{subsec:photothermal}). A fraction of the transmitted laser beam is
read with Thorlabs PDA10CS DC coupled photodiodes and sent to the Intensity
Stabilization Servo (ISS). The ISS primarily consists of an AC coupled
pole-zero pair at 300 Hz and high gain to suppress the laser intensity noise in
the measurement frequency band. A New Focus 4104NF Electro-Optic Amplitude
Modulator (EOAM) is used in each path with a polarizing beam splitter (PBS) to
create an effective intensity actuator. This brings down the relative intensity
noise to $4 \times 10^{-8}$ at 300 Hz.

Additionally, to ensure no higher-order modes are reflected from the cavity and
pollute the FSS PDH error signal, the laser beams are first spatially and
spectrally cleaned by a Pre-Mode Cleaner (PMC). Our PMC is a triangular cavity
with a PZT-actuated end mirror. The length of the PMC is locked to the laser
frequency using standard PDH locking through the PMC servo to ensure only clean
$\text{TEM}_{00}$ are passed on to our test cavities and laser frequency noise
above 2 MHz (PMC optical pole frequency) is rejected. As an added advantage,
the PMC also provides polarization filtering of the laser reducing residual
amplitude modulation due to downstream EOMs used in FSS.
