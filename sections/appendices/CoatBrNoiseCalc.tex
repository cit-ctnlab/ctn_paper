\section{Coating Brownian Noise calculations}
\label{app:coatBr}
First reflectivity of interface from $(j-1)^{th}$ to $j^{th}$ layer is
calculated \cite[Eq. 97]{Hong2013}:
\begin{equation}
r_j = \frac{n_j - n_{j+1}}{n_j - n_{j+1}}
\end{equation}
Here, $j=-1$ is the vacuum, $j=0$ first layer, and so on. $j=N-1$ is the last
layer with $N$ layers and $j=N$ is the substrate. And one-way phase shift that
occurs in light one passing from $j^{th}$ layer is defined as:
\begin{equation}
\phi_j = 2 \pi \frac{d_j n_j}{\lambda}
\end{equation}
From these parameters, we defined Transmission Matrix from $j^{th}$ layer and
Reflection Matrix at interface from $(j-1)^{th}$ to $j^{th}$ layer as
\cite[Eq. 98, 99]{Hong2013}:
\begin{subequations}
\begin{align}
\mathbf{R_j} &= \frac{1}{\sqrt{1 - r_j^2}}\begin{pmatrix}
                                                1 & -r_j \\ -r_j & 1
                                          \end{pmatrix} \\
\mathbf{T_j} &= \begin{pmatrix}
                        e^{i \phi_j} & 0 \\ 0 & e^{-i \phi_j}
                \end{pmatrix}
\end{align}
\end{subequations}
These matrices transform onward and backward-going fields from the left side of
an optical element to the right side of it. Multiplying these matrices in order
of their appearance to light coming from left, we get the total transformation
matrix of the coating as \cite[Eq. 101]{Hong2013}:
\begin{equation}
\mathbf{M} = \mathbf{R_N} \mathbf{T_{N-1}} \mathbf{R_{N-1}}
              ...
              \mathbf{R_1} \mathbf{T_0} \mathbf{R_0}
\end{equation}
Then the complex reflectivity of the entire coating stack can be written as
\cite[Eq. 102]{Hong2013}:
\begin{equation}
\rho = - M_{21} / M_{22}
\end{equation}

To calculate derivatives of this complex reflectivity, we define $\rho_k$ as
the complex reflectivity for $k$ layered stack with layers from $(N-k+1)^{th}$
layer to $N$th layer on top of the same substrate but the $(N-k)^{th}$ layer
material on top instead of air. Then the total transformation matrix for this
k-layered stack would be:
\begin{equation}
\begin{aligned}
\mathbf{M_k} &= \mathbf{R_N} \mathbf{T_{N-1}} \mathbf{R_{N-1}}
               ...
               \mathbf{R_{N-k+1}} \mathbf{T_{N-k}} \mathbf{R_{N-k}}\\
\rho_k &= - \mathbf{M_k}_{21} / \mathbf{M_k}_{22}
\end{aligned}
\end{equation}
One can identify easily that:
\begin{equation}
\mathbf{M_{k+1}} = \mathbf{M_{k}} \mathbf{T_{N-k-1}} \mathbf{R_{N-k-1}}
\end{equation}
This allows us to write a recursion relation for $\rho_{k+1}$ as:
\begin{equation}
\rho_{k+1} = \frac{r_{N-k-1}+\rho _k e^{2 i \phi _{N-k-1}}}
                {1 + r_{N-k-1} \rho _k e^{2 i \phi _{N-k-1}}}
,\quad \rho_{0} = r_N
\end{equation}
We can use this to calculate the following derivatives:
\begin{equation}
\begin{aligned}
\frac{\partial\rho_{k+1}}
     {\partial\rho_k} &= \frac{\left(1 - r_{-k+n-1}^2-1\right)
                               e^{2 i \phi _{N-k-1}}}
                              {\left(1+\rho _k r_{-k+n-1}
                                     e^{2 i \phi _{-k+n-1}}
                               \right)^2}\\
\frac{\partial\rho_{k+1}}
     {\partial r_{N-k-1}} &= \frac{1-\rho _k^2 e^{4 i \phi _{-k+n-1}}}
                                  {\left(1+\rho _k r_{-k+n-1}
                                         e^{2 i \phi _{-k+n-1}}
                                   \right)^2}\\
\frac{\partial\rho_{k+1}}
     {\partial\phi_{N-k-1}} &= -2i\frac{\left(1 - r_{-k+n-1}^2-1\right)
                                        e^{2 i \phi _{N-k-1}}}
                                       {\left(1+\rho _k r_{-k+n-1}
                                              e^{2 i \phi _{-k+n-1}}
                                        \right)^2}
\end{aligned}
\end{equation}
From here, it is straightforward to calculate derivates of $\rho = \rho_{N}$
by using (for $j>1$):
\begin{equation}
\begin{aligned}
\frac{\partial \rho}
     {\partial r_j} &= \left(\prod_{k=N-j}^{N-1} \frac{\partial\rho_{k+1}}
                                                         {\partial\rho_k}
                       \right)
                       \frac{\partial\rho_{N-j}}
                            {\partial r_{j}}\\
\frac{\partial \rho}
     {\partial \phi_j} &= \left(\prod_{k=N-j}^{N-1} \frac{\partial\rho_{k+1}}
                                                         {\partial\rho_k}
                          \right)
                          \frac{\partial\rho_{N-j}}
                               {\partial\phi_{j}}\\
\frac{\partial \rho}
     {\partial r_0} = 1
&,\quad
\frac{\partial \rho}
     {\partial\phi_0} = 0
\end{aligned}
\end{equation}

Once these derivatives are calculated, we define $\epsilon_j(z)$ for each layer
as \cite[Eq. 25]{Hong2013}:
\begin{equation}
\begin{aligned}
\epsilon_j(z) &= (n_j + \beta_j)
                 \frac{\partial log(\rho)}{\partial \phi_j} \\
              &\quad
                 - \beta_j \left[
                             \frac{1 - r_j^2}{2r_j}
                             \frac{\partial log(\rho)}{\partial \phi_j}
                             - \frac{1 + r_j^2}{2r_j}
                               \frac{\partial log(\rho)}{\partial \phi_{j+1}}
                          \right] \\
              &\quad
                   \times cos[2k_0n_j(z-z_{j+1})] \\
              &\quad
                 - t_j^2 \beta_j \frac{\partial log(\rho)}{\partial r_j}
                   sin[2k_0n_j(z-z_{j+1})]
\end{aligned}
\end{equation}
where $z_{j}$ is the position of interface between $j^{th}$ to $(j-1)^{th}$
layers, given by \cite[Eq. 21, ref Fig.1.]{Hong2013}:
\begin{equation}
z_{j} = \sum_{n=j}^N l_n
\end{equation}

Further, we calculate the transfer functions from bulk and shear noise fields
in the coating materials to layer thickness and surface height
\cite[Table. I.]{Hong2013}:
\begin{subequations}
\begin{align}
C_j^B     &= \sqrt{\frac{1+\sigma_j}{2}} \\ C_j^{S_A} &= \sqrt{1-2\sigma_j} \\
D_j^B     &= \frac{1-\sigma_s-2\sigma_s^2}{\sqrt{2(1+\sigma_j)}}
             \frac{Y_j}{Y_s} \\
D_j^{S_A} &= -\frac{1-\sigma_s-2\sigma_s^2}{2\sqrt{1-2\sigma_j}}
              \frac{Y_j}{Y_s} \\
D_j^{S_B} &= \frac{\sqrt{3}(1-\sigma_j)(1-\sigma_s-2\sigma_s^2)}
                  {2\sqrt{1-2\sigma_j}(1+\sigma_j)}
             \frac{Y_j}{Y_s}
\end{align}
\end{subequations}
Finally, we define transfer functions from each layer's bulk and shear stress
noise fields to phase noise of reflected light \cite[Eq. 94]{Hong2013}:
\begin{subequations}
\begin{align}
q_j^B  &= \int\limits_{z_{j+1}}^{z_j}
          \frac{dz}{\lambda_j}
          \left[
              \left[
                  1 - Im \left(\frac{\epsilon_j(z)}{2}\right)
              \right] C_j^B
              + D_j^B
          \right]^2 \\
q_j^S  &= \int\limits_{z_{j+1}}^{z_j}
          \frac{dz}{\lambda_j}
          \left[
              \left[
                  1 - Im \left(\frac{\epsilon_j(z)}{2}\right)
              \right] C_j^{S_A}
              + D_j^{S_A}
          \right]^2 + [D_j^{S_B}]^2
\end{align}
\end{subequations}
