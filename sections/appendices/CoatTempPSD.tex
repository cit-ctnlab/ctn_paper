\section{Temperature Fluctuations PSD}
\label{app:tempPSD}
Here we calculate the PSD for temperature fluctuations of the mirror assuming
that the coating is very thin and transfers all heat immediately to the
substrate. This assumption is taken care of by incorporating thick coating
correction as shown in App.~\ref{app:coatTO} during the calculation of
thermo-optic noise.

Braginsky et al.\cite[Eq. 9]{Braginsky2000} calculated the general one-sided
power spectral density of temperature fluctuations sensed by a gaussian profile
beam affecting thermo-refractive noise as:
\begin{equation}
\begin{aligned}
S^{\Delta T} &= \frac{8 K_B T^2 f_\text{T}^2 \pi^2 w^4}{\kappa_\text{s}}
                \int_0^\infty \mathrm{d} k_{\perp}
                              \frac{2 \pi k_{\perp}}{(2\pi)^2}\\
                &\quad \times
                \int_{-\infty}^{\infty} \frac{\mathrm{d} k_z}{2\pi}
                               \frac{k^2_z + k^2_{\perp}}
                                    {f_\text{T}^2 \pi^2 w^4
                                     (k^2_z + k^2_{\perp})^2
                                     + \omega^2}
                               e^{-k_{\perp}^2 w^2 / 4}\\
                &\quad \quad \quad \times
                               \left(
                                    \frac{1}{1 + k^2_z l^2_\text{pen}}
                               \right)
\end{aligned}
\end{equation}
where $f_\text{T}$ is thermal relaxation frequency (frequency at which thermal
diffusion length becomes $\sqrt{\pi w^2}$), $l_{pen}$ is the penetration depth
of light into the coating, and the assumption of a semi-infinite mirror has
been taken. Note that all material properties are of the substrate as the
coating is assumed to be of negligible thickness in this calculation.

With light penetrating only a few of the first few layers, $l_{pen}$ is much
smaller in comparison to beam spot radius $w$. Note that for $k_z$ to be around
$1/l_{pen}$ or more, where the rightmost term in parenthesis will become any
significant, the denominator in the first term in the integrand will be around
$f_\text{T}^2 \pi^2 w^4/l_{pen}^4 + \omega^2$ and since $l_{pen} << w$, such
values of $k_z$ would not contribute to the integral. Therefore, we can safely
ignore the last term in parenthesis as unity. With this assumption, the
difference between the thermorefractive and thermoelastic effect of the
temperature PSD vanishes and we can use the same PSD for both channels as done
in Eq.~\ref{eq:coatTO}.

Making substitutions $u = k_\perp w/\sqrt{2}$ and complex variable $z = k_z
w/\sqrt{2}$, we get:
\begin{equation}
\begin{aligned}
S^{\Delta T} &= \frac{2 K_B T^2 f_\text{T}^2 w^4}{\kappa_\text{s}}
                \int_0^\infty \mathrm{d} u \frac{2}{w^2} u e^{-u^2/2}\\
             &\quad \times
                \int_{-\infty}^{\infty} \mathrm{d} z \frac{2\sqrt{2}}{w^3}
                               \frac{z^2 + u^2}
                                    {4 \pi^2 f_\text{T}^2
                                     (z^2 + u^2)^2
                                     + \omega^2}\\
            &= \frac{2\sqrt{2}K_B T^2}{\pi^2 \kappa_\text{s} w}
               \int_0^\infty \mathrm{d} u u e^{-u^2/2}
               \int_{-\infty}^{\infty} \mathrm{d} z
                               \frac{z^2 + u^2}
                                    {(z^2 + u^2)^2
                                     + \Omega^2}
\end{aligned}
\end{equation}
where $\Omega = \omega/(2 \pi f_\text{T}) = f/f_\text{T})$.

Instead of assuming the thermal diffusion length to be much smaller than the
beam spot radius, as generally done in large beam spot calculations, we
calculate the $z$ integral analytically as complex contour integral
\cite[G.2]{TaraThesis}:
\begin{equation}
\begin{aligned}
\int_{-\infty}^{\infty} \mathrm{d} z \frac{z^2 + u^2}{(z^2 + u^2)^2+ \Omega^2}
      &= \int_{-\infty}^{\infty} \mathrm{d} z
                        \frac{z^2 + u^2}
                        {\left((z^2 + u^2)+ i\Omega\right)
                         \left((z^2 + u^2)- i\Omega\right)}\\
      &= \int_{-\infty}^{\infty} \mathrm{d} z
                        \frac{z^2 + u^2}
                             {(z - z_1)(z - z_2)(z - z_3)(z - z_4)}
\end{aligned}
\end{equation}
where
\begin{equation}
\begin{aligned}
z_1 &= i\sqrt{u^2 - i\Omega} \\ z_2 &= i\sqrt{u^2 + i\Omega})  \\ z_3 &= -
i\sqrt{u^2 - i\Omega}) \\ z_4 &= - i\sqrt{u^2 + i\Omega})
\end{aligned}
\end{equation}
are the poles of the integrand with subscripts denoting the quadrant of the
complex plane they lie in.

\input{sections/figureTex/contour_integral}

Now, we imagine a closed integral of the integrand as shown by the dotted curve
in fig.\ref{fig:contour_integral} which does not enclose any poles and hence
must be zero. The line integral along $S_l$ is the part that we are trying to
calculate. For a closed curve:
\begin{equation}
\begin{aligned}
\oint \mathrm{d} z\, g(z) &= 0\\
0 &= \int_{S_1} \mathrm{d} z\, g(z) + \int_{S_2} \mathrm{d} z\, g(z)
     + \int_{S_l} \mathrm{d} z\, g(z) + \int_{S_c} \mathrm{d} z\, g(z)\\
\int_{S_l} \mathrm{d} z\, g(z) &= - \int_{S_1} \mathrm{d} z\, g(z)
                              - \int_{S_2} \mathrm{d} z\, g(z)
\end{aligned}
\end{equation}

Integral around curved path $S_c$ goes to zero as it happens at infinity where
the integrand vanishes. The remaining integrals can be easily calculated using
Cauchy's residue theorem (note the negative sign due to the clockwise direction
of curves $S_1$ and $S_2$) as:
\begin{equation}
\begin{aligned}
\int_{S_1} \mathrm{d} z\, g(z) &= -2 \pi i
                                  \frac{u^2 + z_1^2}
                                       {(z_1 - z_2)(z_1 - z_3)(z_1 - z_4)}\\
                               &= \frac{-\pi}
                                       {2 \sqrt{u^2 - i\Omega}}
\end{aligned}
\end{equation}
and
\begin{equation}
\begin{aligned}
\int_{S_2} \mathrm{d} z\, g(z) &= -2 \pi i
                                  \frac{u^2 + z_2^2}
                                       {(z_2 - z_1)(z_2 - z_3)(z_2 - z_4)}\\
                               &= \frac{-\pi}
                                       {2 \sqrt{u^2 + i\Omega}}
\end{aligned}
\end{equation}

Therefore, we have:
\begin{equation}
\begin{aligned}
\int_{-\infty}^{\infty} \mathrm{d} z \frac{z^2 + u^2}{(z^2 + u^2)^2+ \Omega^2}
      &= \frac{\pi}
              {2 \sqrt{u^2 - i\Omega}}
         + \frac{\pi}
                {2 \sqrt{u^2 + i\Omega}}\\
      &= Re \left[ \frac{\pi}{\sqrt{u^2 - i\Omega}}\right]
\end{aligned}
\end{equation}
From here, we reach the final expression as shown in Eq.~\ref{eq:tempPSD}.
