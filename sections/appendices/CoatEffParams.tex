\section{Coating Effective Parameters}
\label{app:coatTO}
Effective thermal conductivity for the coating is defined as the harmonic
weighted mean of individual layer conductivity:
\begin{equation}
\frac{1}{\kappa_\text{c}} = \sum_j \frac{1}{\kappa_j} \frac{d_j}{d}
\end{equation}

Effective coefficients of thermoelasticity for the coating and substrate are
\cite[Eq. A2]{Evans2008}:
\begin{subequations}
\begin{align}
\bar{\alpha_j}        &= \alpha_j
                         \frac{1+\sigma_\text{s}}
                              {1-\sigma_j}
                         \left[
                              \frac{1+\sigma_j}{1+\sigma_\text{s}}
                              + (1-2\sigma_\text{s})\frac{Y_j}{Y_\text{s}}
                         \right] \\
\bar{\alpha_\text{c}} &= \sum_j \bar{\alpha_j} \frac{d_j}{d} \\
\bar{\alpha_\text{s}} &= 2\alpha_\text{s} (1 + \sigma_\text{s})
\end{align}
\end{subequations}
These can be used to define a condensed effective CTE given by
\cite[Eq.18]{Evans2008}:
\begin{equation}
\bar{\Delta\alpha} = \bar{\alpha_\text{c}}
                     - \bar{\alpha_\text{s}} d \frac{C_\text{c}}{C_\text{s}}
\end{equation}
where $C_\text{c}$ is the effective heat capacity per volume of coating
calculated by taking a weighted average of all coating layers as:
\begin{equation}
C_\text{c} = \sum_j C_j \frac{d_j}{d}
\end{equation}

The effective coefficient of thermorefractivity is given by
\cite[Eq.B8-B21]{Evans2008}:
\begin{equation}
\bar{\beta} = -\frac{1}{4\pi} \sum\limits_{k=1}^{N}
               \frac{\partial \phi_c}{\partial \phi_k}
               \frac{\partial \phi_k}{\partial T}
\end{equation}
where
\begin{subequations}
\begin{align}
\frac{\partial \phi_c}{\partial \phi_k} &=
                    -\frac{1}{2} \frac{\partial \rho}{\partial \phi_j} \\
\frac{\partial \phi_k}{\partial T} &=
                    \frac{4 \pi}{\lambda}
                    (\beta_k + \bar{\alpha_k}n_k)d_k
\end{align}
\end{subequations}

Note, that we utilized the calculation of derivatives of complex reflectivity
of the coating done for coating Brownian noise in this calculation as well.
The factor of $-1/2$ comes because of different definitions of phase shift
between \cite{Evans2008} and \cite{Hong2013}.

We have also considered the non-homogeneous dissipation of
heat in the coatings due to finite thickness. This is incorporated by a thick
coating correction factor $\Gamma_{tc}$ given by \cite[Eq.39]{Evans2008}:
\begin{subequations}
\begin{align}
\Gamma_\text{tc} &= \frac{p_\text{E}^2 \Gamma_0
                          + p_\text{E} p_\text{R} \Xi \Gamma_1
                          + p_\text{R}^2 \Xi^2 \Gamma_2}
                         {R\Xi^2 \Gamma_D} \\
\Gamma_0         &= 2(\sinh\Xi - \sin\Xi) + 2R(\cosh\Xi - \cos\Xi) \\
\Gamma_1         &= 8\sin(\Xi/2)\,[r_T \cosh(\Xi/2) + \sinh(\Xi/2)] \\
\Gamma_2         &= (1+R^2)\sinh(\Xi) + (1-R^2)\sin(\Xi) + 2 R \cosh(\Xi) \\
\Gamma_D         &= (1+R^2)\cosh(\Xi) + (1-R^2)\cos(\Xi) + 2 R \sinh(\Xi)
\end{align}
\end{subequations}
where $p_\text{E}$ and $p_\text{R}$ are power deposition fractions for
thermoelasticity and thermorefractivity respectively, given by
\cite[Eq. 42]{Evans2008}:
\begin{equation}
p_\text{E} = \frac{\bar{\Delta\alpha}\ d}
                  {\bar{\Delta\alpha}\ d-\beta_\text{c}\lambda}
\quad\text{and}\quad
p_\text{R} = \frac{-\beta_\text{c}\lambda}
                  {\bar{\Delta\alpha}\ d-\beta_\text{c}\lambda}
\end{equation}
and $R$ and $\Xi$ are dimensionless scaling factors given by
\cite[Eq. 36, 41]{Evans2008}:
\begin{equation}
R = \sqrt{\frac{\kappa_\text{c} C_\text{c}}{\kappa_\text{s} C_\text{s}}}
\quad\text{and}\quad
\Xi = d \sqrt{\frac{4\pi f C_\text{c}}{\kappa_\text{c}}}
\end{equation}
