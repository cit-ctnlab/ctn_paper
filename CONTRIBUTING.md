* Please limit any line in any file to less than 80 characters. Latex ignores
all single carriage returns and hence all lines can be limited to this length.
* Please write mathematical statements assuming PEP8 guidelines. Space around
each operator and proper indentation in multiline expressions.
* All citations are made with keys, "AuthorLastNameYYYY". Ex: Hong2013.
* Due to enormous number of symbols, I'm collecting all symbols, their
definitions and value used in table tab:nb_symbols. This would save effort on
defining symbols in a scattered way throughout the paper.
* Run make once locally before committing and pushing your changes. You can in
most cases catch compilation error instead of failing CI from your push.